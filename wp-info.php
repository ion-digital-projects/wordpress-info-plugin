<?php
/*
  Plugin Name: WP Info
  Version: 0.0.2
  Plugin URI: https://www.wpsolved.io
  Author: Justus Meyer
  Author URI: https://www.wpsolved.io
  Description: WP Info displays useful information on your WordPress installation to aid in debugging issues.
  Text Domain: wp-info
  Domain Path: /languages
 */

function wpInfoPhpInfo() {
    phpinfo();
    wp_die();
}

function wpInfoAdminHead() {
    ?>
    <style>
        
        #wp-info-phpinfo div {
            
            width: 100%; 
            height: 100%;
            margin-left: auto; 
            margin-right: auto;                
        }
        
        #wp-info-phpinfo > div {

            height: 600px;
        }

        #wp-info-phpinfo > div > div {

            /* empty! */
        }

        #wp-info-phpinfo > div > div > div {

            margin-top: 16px;
            margin-bottom: 16px;
            border: 1px solid rgb(204, 204, 204);
            background-color: #ffffff;
        }

        #wp-info-phpinfo > div > div > div > iframe {

            width: 100%;
            height: 100%;
        }


    </style>
    <?php
}

function wpInfoAdminInit() {
    
    add_action('wp_ajax_wp-info-phpinfo', 'wpInfoPhpInfo');
    
    add_action('admin_head', 'wpInfoAdminHead');
    
    add_submenu_page('tools.php', "WP Info", "Info", 'manage_options', 'wp-info', function() {
        ?>
        
<div id="wp-info-phpinfo" class="wrap" style="">    
    <h2 class="nav-tab-wrapper">            
        <a href="?page=wp-info" class="nav-tab nav-tab-active">PHP Info</a>
    </h2>
    <div>
        <div>
            <div>
                <iframe src="<?php echo get_admin_url(); ?>admin-ajax.php?action=wp-info-phpinfo">

                </iframe>                
            </div>            
        </div>
    </div>

</div>

        <?php
    }); 
}

add_action('admin_init', 'wpInfoAdminInit');
